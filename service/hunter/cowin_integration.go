package hunter

import (
	"time"
	"net/http"
	"io/ioutil"
	"strconv"
	"encoding/json"
)

var DistrictCodeMappings = map[string]int {
	"Kolkata": 725,
	"Bhopal" : 312,
	"Gurgaon": 188,
	"EastDelhi": 145,
	"KamrupRural": 50,
	"Indore": 314,
	"KamrupMetropolitian": 49,
	"Cuttack": 457,
	"Patna": 97,
	"Faridabad": 199,
	"Shahdara": 148,
}

const LOOK_AHEAD_IN_DAYS = 7

func getSlotsForDistrict(districtName string) map[string] CentreSlots {
	districtCode := DistrictCodeMappings[districtName]
	if districtCode == 0 { return nil }

	var slotsInDistrictAggregate = map[string] CentreSlots { }
	for dayIter := 0; dayIter < LOOK_AHEAD_IN_DAYS ; dayIter++ {
		formattedQueryDate := time.Now().AddDate(0, 0, dayIter).Format("02-01-2006")
		slotsInDistrict := getCowinData(strconv.Itoa(districtCode), formattedQueryDate)
		slotsInDistrictAggregate[formattedQueryDate] = slotsInDistrict
	}
	return slotsInDistrictAggregate
}


func getCowinData(distictCode string, queryDate string) CentreSlots {
	client := &http.Client{}
	queryUrl := "https://cdn-api.co-vin.in/api/v2/appointment/sessions/public/calendarByDistrict?district_id="+ distictCode + "&date="+ queryDate
	req, err := http.NewRequest("GET", queryUrl, nil)
	if err != nil {
		return CentreSlots{}
	}
	req.Header.Set("User-Agent", "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36")
	resp, err := client.Do(req)
	if err != nil {
		return CentreSlots{}
    }
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
			return CentreSlots{}
	}

	var centreSlots CentreSlots	
	json.Unmarshal([]byte(string(body)), &centreSlots)
	return centreSlots
}
