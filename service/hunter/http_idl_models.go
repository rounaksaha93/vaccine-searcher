package hunter

// HunterParams ... params to hunt vaccines
type HunterParams struct {
	ApiKey            string
	Beneficiaries     []Beneficiary
	ApprovedDistricts []string
}

type Beneficiary struct {
	BeneName      string
	BeneNumber    string
	BeneDistricts []string
	Email string
}

type CentreSlots struct {
	Centers []CentreDetails
}

type CentreDetails struct {
	CenterID     int    `json:"center_id"`
	Name         string `json:"name"`
	Address      string `json:"address"`
	StateName    string `json:"state_name"`
	DistrictName string `json:"district_name"`
	BlockName    string `json:"block_name"`
	Pincode      int    `json:"pincode"`
	Lat          int    `json:"lat"`
	Long         int    `json:"long"`
	From         string `json:"from"`
	To           string `json:"to"`
	FeeType      string `json:"fee_type"`
    Sessions     []SessionDetails `json:"sessions"`
}

type SessionDetails struct {
	SessionID         string   `json:"session_id"`
	Date              string   `json:"date"`
	AvailableCapacity int      `json:"available_capacity"`
	MinAgeLimit       int      `json:"min_age_limit"`
	Vaccine           string   `json:"vaccine"`
	Slots             []string `json:"slots"`
	AvailableCapacityDose1 int      `json:"available_capacity_dose1"`
	AvailableCapacityDose2 int      `json:"available_capacity_dose2"`
}

type AvailableCenterDetails struct {
	CentreName string
	Capacity int
	VaccineName string
	Pincode int
	Date string
	AvailableCapacityDose1 int
	AvailableCapacityDose2 int
	MinAgeLimit       int
	AvailableCapacity int
}