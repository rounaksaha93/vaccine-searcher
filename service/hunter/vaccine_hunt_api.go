package hunter

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"strconv"
	"time"
	configs "vaccine-searcher/configs"
)

//Hunt ... Hunts Vaccines for Beneficiaries
func Hunt(c *gin.Context) {
	// Bind Request Body to FileParseReq Definition. Terminate flow on error
	var hunterParams HunterParams
	if err := c.ShouldBindJSON(&hunterParams); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	// API Auth
	if hunterParams.ApiKey != configs.ApiSecretKey {
		c.JSON(http.StatusBadRequest, gin.H{"error": "UNAUTHORISED"})
		return
	}

	go initiateHuntAndCommunication(c, hunterParams)
	c.JSON(200, gin.H{"isError": false, "message": "Hunt has been initiated. WhatsApps will be triggered soon!"})
}

//Hunt ... Hunts Vaccines for Beneficiaries who are 45+
func Hunt45Plus(c *gin.Context) {
	// Bind Request Body to FileParseReq Definition. Terminate flow on error
	var hunterParams HunterParams
	if err := c.ShouldBindJSON(&hunterParams); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	// API Auth
	if hunterParams.ApiKey != configs.ApiSecretKey {
		c.JSON(http.StatusBadRequest, gin.H{"error": "UNAUTHORISED"})
		return
	}

	go initiateHuntAndCommunication45Plus(c, hunterParams)
	c.JSON(200, gin.H{"isError": false, "message": "Hunt has been initiated. WhatsApps will be triggered soon!"})
}

func initiateHuntAndCommunication(c *gin.Context, hunterParams HunterParams) {
	var vaccinationSlots map[string]map[string]CentreSlots = getVaccinationSlots(hunterParams.ApprovedDistricts)
	flattenedOutVaccinationSlots := get18PlusVacciationDetailsForDistricts(vaccinationSlots)
	go initiateCommsToBenes(hunterParams, flattenedOutVaccinationSlots, 18)
}

func initiateHuntAndCommunication45Plus(c *gin.Context, hunterParams HunterParams) {
	var vaccinationSlots map[string]map[string]CentreSlots = getVaccinationSlots(hunterParams.ApprovedDistricts)
	flattenedOutVaccinationSlots := get45PlusVacciationDetailsForDistricts(vaccinationSlots)
	go initiateCommsToBenes(hunterParams, flattenedOutVaccinationSlots, 45)
}

func initiateCommsToBenes(hunterParams HunterParams, districtWiseSlotMaps map[string][]AvailableCenterDetails, minAge int) {
	for _, beneficiary := range hunterParams.Beneficiaries {
		for _, district := range beneficiary.BeneDistricts {
			if districtWiseSlotMaps[district] != nil {
				sendCommunication(beneficiary, district, districtWiseSlotMaps[district], minAge)
			}
		}
	}
}

func sendCommunication(beneficiary Beneficiary, district string, distirctSlotDetails []AvailableCenterDetails, minAge int) {
	var emailBody string = ""
	var messageHeader string = "VACCINE SLOTS FOR DISTRICT " + district + " !! \n\n"
	emailBody = emailBody + messageHeader
	emailBody = emailBody + "MIN AGE :: " + strconv.Itoa(minAge) + " !! \n\n"
	for _, slots := range distirctSlotDetails {
		var slotString string = "CENTER NAME : " + slots.CentreName + "\n"
		slotString = slotString + "CAPACITY FOR DOSE 1 : " + strconv.Itoa(slots.AvailableCapacityDose1) + "\n"
		slotString = slotString + "CAPACITY FOR DOSE 2 : " + strconv.Itoa(slots.AvailableCapacityDose2) + "\n"
		slotString = slotString + "VACCINE : " + slots.VaccineName + "\n"
		slotString = slotString + "PINCODE : " + strconv.Itoa(slots.Pincode) + "\n"
		slotString = slotString + "DATE : " + slots.Date + "\n"

		emailBody = emailBody + slotString + "\n\n"
	}
	beneEmail := []string{
		beneficiary.Email,
	}
	time.Sleep(1 * time.Second)
	go sendEmail(beneEmail, emailBody)
}

// TODO : Fix these loopings. At least make loops in parallel.
func get18PlusVacciationDetailsForDistricts(vaccinationAvailabilityMap map[string]map[string]CentreSlots) map[string][]AvailableCenterDetails {
	var districtWiseFlattenedOutSlots = map[string][]AvailableCenterDetails{}
	for district, datedCentreDetails := range vaccinationAvailabilityMap {
		var availableSlotsInDistrict []AvailableCenterDetails
		for _, centreDetails := range datedCentreDetails {
			for _, centers := range centreDetails.Centers {
				for _, sessions := range centers.Sessions {
					if sessions.MinAgeLimit == 18 && sessions.AvailableCapacityDose1 > 25 {
						var availableCenter AvailableCenterDetails
						availableCenter.VaccineName = sessions.Vaccine
						availableCenter.Capacity = sessions.AvailableCapacity
						availableCenter.Date = sessions.Date
						availableCenter.CentreName = centers.Name
						availableCenter.Pincode = centers.Pincode
						availableCenter.AvailableCapacityDose1 = sessions.AvailableCapacityDose1
						availableCenter.AvailableCapacityDose2 = sessions.AvailableCapacityDose2
						availableCenter.AvailableCapacity = sessions.AvailableCapacity
						availableCenter.MinAgeLimit = sessions.MinAgeLimit
						availableSlotsInDistrict = append(availableSlotsInDistrict, availableCenter)
					}
				}
			}
		}
		districtWiseFlattenedOutSlots[district] = availableSlotsInDistrict
	}
	return districtWiseFlattenedOutSlots
}

// TODO : Fix these loopings. At least make loops in parallel.
func get45PlusVacciationDetailsForDistricts(vaccinationAvailabilityMap map[string]map[string]CentreSlots) map[string][]AvailableCenterDetails {
	var districtWiseFlattenedOutSlots = map[string][]AvailableCenterDetails{}
	for district, datedCentreDetails := range vaccinationAvailabilityMap {
		var availableSlotsInDistrict []AvailableCenterDetails
		for _, centreDetails := range datedCentreDetails {
			for _, centers := range centreDetails.Centers {
				for _, sessions := range centers.Sessions {
					if sessions.MinAgeLimit == 45 && sessions.AvailableCapacityDose2 > 25 && sessions.Vaccine == "COVAXIN" {
						var availableCenter AvailableCenterDetails
						availableCenter.VaccineName = sessions.Vaccine
						availableCenter.Capacity = sessions.AvailableCapacity
						availableCenter.Date = sessions.Date
						availableCenter.CentreName = centers.Name
						availableCenter.Pincode = centers.Pincode
						availableCenter.AvailableCapacityDose1 = sessions.AvailableCapacityDose1
						availableCenter.AvailableCapacityDose2 = sessions.AvailableCapacityDose2
						availableCenter.AvailableCapacity = sessions.AvailableCapacity
						availableCenter.MinAgeLimit = sessions.MinAgeLimit
						availableSlotsInDistrict = append(availableSlotsInDistrict, availableCenter)
					}
				}
			}
		}
		districtWiseFlattenedOutSlots[district] = availableSlotsInDistrict
	}
	return districtWiseFlattenedOutSlots
}

func getVaccinationSlots(approvedDistricts []string) map[string]map[string]CentreSlots {
	var DistrictSlots = map[string]map[string]CentreSlots{}
	for _, district := range approvedDistricts {
		slotsInDistrict := getSlotsForDistrict(district)
		DistrictSlots[district] = slotsInDistrict
	}
	return DistrictSlots
}
