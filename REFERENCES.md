MAC OS Cron : env EDITOR=nano crontab -e

Build Command for AWS : 
env GOOS=linux GOARCH=amd64 go build 


SCP Command : 
scp -i "vaccine-searcher-main.pem" /Users/ronaksaha/go_repos/src/vaccine-searcher/vaccine-searcher ec2-user@ec2-65-1-100-19.ap-south-1.compute.amazonaws.com:~/.

Handy Commands : 
sudo netstat -plnt | grep 9999

AWS Connect : 
ssh -i "vaccine-searcher-main.pem" ec2-user@ec2-65-1-100-19.ap-south-1.compute.amazonaws.com


CRON Commands : 
service crond status
sudo crontab -l
sudo crontab -e [Use i to insert and then wq to exit]

*/2 * * * * /home/ec2-user/bash_script_hunter.sh
*/30 * * * * /home/ec2-user/healthcheck.sh