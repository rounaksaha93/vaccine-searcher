curl --location --request POST 'http://localhost:9999/huntVaccines' \
--header 'Content-Type: application/json' \
--data-raw '{
    "apiKey": "secret123(())**&^^",
    "beneficiaries": [
        {
            "beneName": "Rounak Saha",
            "beneNumber": "+919711157590",
            "email": "saharounak1993@gmail.com",
            "beneDistricts": ["Kolkata", "Gurgaon", "KamrupRural", "Indore", "EastDelhi", "KamrupMetropolitian"]
        }
    ],
    "approvedDistricts": ["Kolkata", "Gurgaon", "EastDelhi", "KamrupRural", "Indore", "KamrupMetropolitian"]
}'