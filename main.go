package main

import (
	"github.com/gin-gonic/gin"
	"strconv"
	configs "vaccine-searcher/configs"
	service "vaccine-searcher/service"
	hunter "vaccine-searcher/service/hunter"
)

func main() {
	router := gin.Default()
	router.GET("/ping", service.HealthCheck)
	router.POST("/huntVaccines", hunter.Hunt)
	router.POST("/huntVaccinesFor45Plus", hunter.Hunt45Plus)

	// Starting the server
	router.Run(":" + strconv.Itoa(configs.ServicePort))
}
